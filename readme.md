# Overview and Goal
An OSC broadcaster's goal is to allow applications providing data to forget about who their consumers are. They simply send their data to a well known central place which takes care of its distribution. This broadcaster maintains a list of all subscribers to whom all incoming OSC packets (apart from control messages) are being forwarded. Any computer from which OSC data is received is automatically registered. Registering is also possible in an explicit way as well as cancelling a subscription, receiving a list of all subscribers or resetting the list of subscribers.

## TL;DR
Install Rust, Cargo, Gem, and fpm (assumes you are using Bash and a Debian derivative):
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain stable --profile complete
echo 'export PATH="$PATH:$HOME/.cargo/bin"' >> ~/.bashrc
source ~/.bashrc

sudo apt-get update
sudo apt-get install -y ruby
sudo gem install fpm
```

Clone the repository:
```bash
git clone https://gitlab.com/Nixname/OSCBroadcaster.git
```

Compile, package and install.

Using 64 bit Intel processor:
```bash
cargo build --target x86_64-unknown-linux-gnu --release
./package.py deb
sudo dpkg -i osc_broadcaster-0.1.0-1-x86_64.deb
```

On Raspberry PI:
```bash
cargo build --target armv7-unknown-linux-gnueabihf --release
./package.py deb
sudo dpkg -i osc_broadcaster-0.1.0-1-armhf.deb
```

Enable and start service:
```bash
sudo systemctl daemon-reload
sudo systemctl enable --now osc-broadcaster.service
```

## Context
This software was created as part of a module at the Fachhochschule Kiel - University of Applied Sciences I, Bjarne Kühl, took part in. It is essentially a port of an [example](https://www.cs.princeton.edu/~prc/ChucKU/Code/OSCAndMIDIExamples/VideoAction/oscP5/examples/oscP5broadcaster/oscP5broadcaster.pde) that was extended with more functionality by the lecturer Prof. Dr. Steffen Prochnow. Porting the application was necessary as the previous solution did not properly support OSC-Bundles and could not be made to without heavily restructuring the underlying library and introducing breaking changes.

## Compatibility
This program is as platform independent as Rust is, so it is possible to compile this program for all major operating systems.
