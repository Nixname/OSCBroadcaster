#!/usr/bin/env python3

import os
import subprocess
import argparse

os.chdir(os.path.dirname(os.path.realpath(__file__)))


parser = argparse.ArgumentParser(description="Package the program")
parser.add_argument("packaging", help="The packaging format to use")

arguments = parser.parse_args()

packagingFormat = arguments.packaging


def read_variables():
    with open(".variables") as file:
        variableMap = dict()

        lines = file.readlines()

        if len(lines) % 3 != 0:
            raise Exception(
                f"Unexpected number of lines.\nlen(lines): {len(lines)}"
            )

        index = 0

        while True:
            varName = lines[index][:-1]

            if len(varName) <= 1:
                raise Exception("Variable name is empty.\nLine: {index}")

            varValue = lines[index + 1][:-1]

            separatorLine = lines[index + 2]
            if len(separatorLine) > 1:
                raise Exception(
                    "Separator line not empty.\n"
                    f"separatorLine: {separatorLine}\n"
                    f"line: {index + 2}"
                )

            variableMap[varName] = varValue

            index = index + 3
            if index >= len(lines):
                break

        return variableMap


def getCommand(packagingFormat, variables):
    resultingArch = variables["CARGO_CFG_TARGET_ARCH"]

    if variables["TARGET"] == "armv7-unknown-linux-gnueabihf":
        resultingArch = "armhf"

    if packagingFormat == "deb":
        packageFileName = f"{variables['CARGO_PKG_NAME']}-" \
            f"{variables['CARGO_PKG_VERSION']}-1-" \
            f"{resultingArch}.deb"
    elif packagingFormat == "pacman":
        packageFileName = f"{variables['CARGO_PKG_NAME']}-" \
            f"{variables['CARGO_PKG_VERSION']}-1-" \
            f"{resultingArch}.pkg.tar.zst"
    else:
        raise Exception(
            f"The packaging format '{packagingFormat}' is not recognised."
        )

    return [
        "fpm",
        "-s",
        "dir",
        "-t",
        packagingFormat,
        "-p",
        packageFileName,
        "--name",
        variables["CARGO_PKG_NAME"],
        "--version",
        variables["CARGO_PKG_VERSION"],
        "--architecture",
        resultingArch,
        "--depends",
        "systemd",
        "--description",
        "Program to receive OSC packets and broadcast them to all listeners",
        "--maintainer",
        "Bjarne Kühl <Bjarne@Kuehl-Kiel.de>",
        f"target/{variables['TARGET']}/"
        f"{variables['PROFILE']}/"
        f"{variables['CARGO_PKG_NAME']}="
        f"/usr/bin/{variables['CARGO_PKG_NAME']}",
        "resources/osc-broadcaster.service="
        "/usr/lib/systemd/system/osc-broadcaster.service",
    ]


variables = read_variables()

command = getCommand(packagingFormat, variables)

subprocess.run(command)
