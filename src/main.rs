//! Based on an example of how to use the Processing library oscP5 at http://www.sojamo.de/oscP5
//! Incorporates changes made by Steffen Prochnow to that example.

use std::{
    collections::HashSet,
    net::{IpAddr, UdpSocket},
};

use const_format::formatcp;
use env_logger::Env;
use log::{debug, error, info, warn};
use rosc::{OscMessage, OscPacket, OscType};
use clap::Parser;

/// listeningPort is the port the server is listening for incoming messages
const MY_LISTENING_PORT: u16 = 9000;
pub const MY_LISTENING_PORT_STR: &str =
    formatcp!("{}", MY_LISTENING_PORT);

/// the broadcast port is the port the clients should listen for incoming messages from the server
const MY_BROADCAST_PORT: u16 = 9001;
pub const MY_BROADCAST_PORT_STR: &str =
    formatcp!("{}", MY_BROADCAST_PORT);

const MY_CONNECT_PATTERN: &str = "/server/connect";
const MY_DISCONNECT_PATTERN: &str = "/server/disconnect";
const MY_LIST_PATTERN: &str = "/server/list";
const MY_RESET_PATTERN: &str = "/server/reset";

/// Receive OSC packets and broadcast them to all listeners
#[derive(Debug, Parser)]
struct Arguments {
    /// The port on which the broadcaster will listen for incoming OSC packets
    #[clap(long, default_value = MY_LISTENING_PORT_STR)]
    pub listening_port: u16,
    /// The port to which OSC packets will be sent
    #[clap(long, default_value = MY_BROADCAST_PORT_STR)]
    pub broadcast_port: u16,
}

fn main() -> Result<(), String> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let arguments = Arguments::parse();

    let recv_socket =
        UdpSocket::bind(("0.0.0.0", arguments.listening_port)).map_err(|error| error.to_string())?;
    info!("UDP server is listening on port {}", arguments.listening_port);

    // Theoretical limitation to UDP content length
    // Does not support IPv6 jumbograms, but that shoudn't pose a problem for now (21.11.2021)
    let mut raw_buffer = [0; 65527];

    let mut my_net_address_list: HashSet<IpAddr> = HashSet::new();

    loop {
        let (number_of_bytes_received, origin) = match recv_socket.recv_from(&mut raw_buffer) {
            Ok(data) => data,
            Err(error) => {
                error!("Error receiving data from the network.\nError: {:#?}", error);

                continue;
            }
        };

        let raw_packet = &raw_buffer[..number_of_bytes_received];

        let osc_packet = match rosc::decoder::decode(raw_packet) {
            Ok(osc_packet) => osc_packet,
            Err(error) => {
                warn!(
                    "Network packet could not be parsed as an OSC packet.\nError: {:#?}",
                    error
                );

                continue;
            }
        };

        debug!("Received OSC packet: {:#?}", osc_packet);

        match osc_packet {
            OscPacket::Bundle(_) => {}
            OscPacket::Message(osc_message) => {
                // Check if the address pattern fits any of our patterns
                match osc_message.addr.as_str() {
                    // Connect to remote location
                    MY_CONNECT_PATTERN => {
                        connect(&mut my_net_address_list, &origin.ip());

                        continue;
                    }
                    // Disconnect remote location
                    MY_DISCONNECT_PATTERN => {
                        disconnect(&mut my_net_address_list, &origin.ip());

                        continue;
                    }
                    // List broadcast remote locations
                    MY_LIST_PATTERN => {
                        statistics(&my_net_address_list);

                        for (index, ip_addr) in my_net_address_list.iter().enumerate() {
                            let new_message = OscMessage {
                                addr: MY_LIST_PATTERN.to_string(),
                                args: vec![
                                    OscType::Int(index as i32),
                                    OscType::String(ip_addr.to_string()),
                                    // Just to be compliant with the behaviour of the Processing solution
                                    OscType::Int(arguments.broadcast_port as i32),
                                ],
                            };

                            let raw_packet_to_be_sent = match rosc::encoder::encode(&OscPacket::Message(
                                new_message,
                            )) {
                                Ok(raw_packet_to_be_sent) => raw_packet_to_be_sent,
                                Err(error) => {
                                    warn!("Could not encode OSC message for sending to listeners.\nError: {:#?}", error);

                                    continue;
                                }
                            };

                            broadcast_data(&arguments, &my_net_address_list, &raw_packet_to_be_sent);

                            info!("Connected: {} Address: {}", index, ip_addr);
                        }

                        continue;
                    }
                    // Reset all remote locations
                    MY_RESET_PATTERN => {
                        info!("Reset server connections");
                        my_net_address_list.clear();
                        statistics(&my_net_address_list);

                        continue;
                    }
                    // Not a pattern meant for the OSC broadcaster
                    _ => {}
                }
            }
        }

        connect(&mut my_net_address_list, &origin.ip());

        broadcast_data(&arguments, &my_net_address_list, raw_packet);
    }
}

fn broadcast_data(arguments: &Arguments, address_list: &HashSet<IpAddr>, raw_data: &[u8]) {
    if !address_list.is_empty() {
        let send_socket = match UdpSocket::bind(("0.0.0.0", 0)) {
            Ok(send_socket) => send_socket,
            Err(error) => {
                error!("Cannot open socket to send data\nError: {:#?}", error);

                return;
            }
        };

        for &ip_addr in address_list {
            match send_socket.send_to(raw_data, (ip_addr, arguments.broadcast_port)) {
                Ok(_) => {}
                Err(error) => {
                    warn!(
                        "Could not send data to receiver: {}\nError: {:#?}",
                        ip_addr, error
                    );
                }
            }
        }
    }
}

fn statistics(address_list: &HashSet<IpAddr>) {
    info!(
        "Currently there are {} remote locations connected.",
        address_list.len()
    );
}

fn connect(address_list: &mut HashSet<IpAddr>, ip_addr: &IpAddr) {
    if !address_list.contains(ip_addr) {
        address_list.insert(*ip_addr);
        info!("Adding {} to the list.", ip_addr);
        statistics(address_list);
    }
}

fn disconnect(address_list: &mut HashSet<IpAddr>, ip_addr: &IpAddr) {
    if address_list.contains(ip_addr) {
        address_list.remove(ip_addr);
        info!("Removing {} from the list.", ip_addr);
        statistics(address_list);
    }
}
